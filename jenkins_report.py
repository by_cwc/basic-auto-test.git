# -*- coding: utf-8 -*-
# @Author  : caiweichao
# @explain : 通过jenkins发送allure测试报告


import os
import sys

import jenkins

from util.basic.analysis_yaml import AnalysisYaml
from util.basic.dingding_request import DingdingRequest


def send_report():
    conf = AnalysisYaml().get_date("JENKINS_CONFIG")
    job_url = conf.get('JOB_URL')
    jenkins_url = conf.get("JENKINS_URL")
    # 实例化jenkins对象
    jenkins_server = jenkins.Jenkins(
        url=jenkins_url,
        username=conf.get("JENKINS_ACCOUNT"),
        password=conf.get("JENKINS_PWD")
    )
    # 获取job最后一次的构建内容
    job_last_bulid = jenkins_server.get_info(job_url)["lastBuild"]["url"]
    # 测试报告地址
    report_url = job_last_bulid + "allure/"
    # 发送报告
    DingdingRequest(robot_name="oper_dingding_robot").send_test_report(report_url=report_url)


if __name__ == '__main__':
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    send_report()
