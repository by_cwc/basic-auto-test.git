# -*- coding: utf-8 -*-
# -------------------------------
# @文件：temporary_data.py
# @时间：2024/4/11 上午10:12
# @作者：caiweichao
# @功能描述：临时数据文件（通过 python 的反射机制在测试运行时存储临时数据的地方）
# -------------------------------


class BasicData:
    cookies = None
    session = None