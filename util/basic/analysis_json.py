# -*- coding: utf-8 -*-
# -------------------------------
# @文件：json_utils.py
# @时间：2024/5/17 下午4:34
# @作者：caiweichao
# @功能描述：解析 json
# -------------------------------
import json
from jsonpath import jsonpath

from util.basic.log import Log
from util.customize_exception import AnalysisError


def analysis_json(json, json_path_rule: str):
    try:
        if jsonpath(json, json_path_rule):
            Log.info(f"json解析结果为{jsonpath(json, json_path_rule)[0]}")
            return jsonpath(json, json_path_rule)[0]
        else:
            raise AnalysisError(f"json解析异常！解析规则:{json_path_rule}")
    except TypeError:
        raise AnalysisError(f"json解析异常！解析规则:{json_path_rule}")
