# -*- coding: utf-8 -*-
# -------------------------------
# @文件：get_env.py
# @时间：2024/3/22 13:59
# @作者：caiweichao
# @功能描述：获取用例运行时候的环境
# -------------------------------
import os
import platform

from util.basic.analysis_yaml import AnalysisYaml


def get_env():
    """
    获取用例运行时候环境的方法，
    1. 如果部署到 jenkins，通过读取 jenkins 的配置参数来指定环境 (参考说明文档README.md)
    2. 如果是本地执行通过 config.yaml中的GLOBAL的值来指定环境
    """
    system_env = True if platform.system() == "Linux" else False
    if system_env:
        # 如果是 linxu 环境读取服务器的环境变量，这个环境变量通过 jenkins 传入
        return os.environ.get("env").lower()
    else:
        return AnalysisYaml().get_date('GLOBAL').lower()
