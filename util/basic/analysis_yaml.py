# -*- coding: utf-8 -*-
# -------------------------------
# @文件：analysis_yaml.py
# @时间：2024/3/26 14:38
# @作者：caiweichao
# @功能描述：解析 yaml 文件工具类
# -------------------------------
from config.constants_path import CONFIG_PATH

import yaml


class AnalysisYaml:
    __obj = None

    @staticmethod
    def __new__(cls, *args, **kwargs):
        if not cls.__obj:
            cls.__obj = super().__new__(cls)
        return cls.__obj

    def __init__(self, file_url=None):
        try:
            # 如果初始化的时候没有传文件路径就读取框架的配置文件
            self.file_url = file_url if file_url else CONFIG_PATH
            with open(file=self.file_url, mode='r', encoding='utf-8') as file:
                self.__yamlData: dict = yaml.load(file.read(), Loader=yaml.FullLoader)
        except Exception as e:
            raise f"yaml文件{file_url}解析异常\n{e}"

    def get_date(self, key=None):
        """
        返回读取的 yaml
        :param key: yaml解析出来的 dict 的 key 值
        :return: 解析后的 yaml 的 value 或者完成的 yaml 文件内容
        """
        return self.__yamlData[key] if key else self.__yamlData

    def get_mysql_config(self, mysql_name):
        mysql_configs = self.get_date(key="MYSQl_CONFIG")
        mysql_config = mysql_configs[mysql_name]
        if mysql_config:
            return mysql_config
        else:
            raise KeyError(f"数据库配置:--  {mysql_config}  --不存在!")

    def get_dingding_robot_config(self, robot_name) -> dict:
        """
        获取钉钉机器人的配置信息
        :param robot_name:机器人的 name
        :return:返回解析后的钉钉机器人的 URL 和 密钥
        """
        robots = self.get_date(key="ROBOT_CONfIG")
        robot_config = robots.get(robot_name)
        if robot_config:
            return robot_config
        else:
            raise KeyError(f"钉钉机器人配置:--  {robot_name}  --不存在!")
