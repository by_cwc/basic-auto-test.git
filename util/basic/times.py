# -*- coding: utf-8 -*-
# -------------------------------
# @文件：times.py
# @时间：2024/3/22 15:30
# @作者：caiweichao
# @功能描述：时间处理工具类
# -------------------------------
import time
import datetime
from datetime import timedelta


class Times:

    # 获取时间戳
    @staticmethod
    def timestamp() -> int:
        return int(time.time())

    # 自定义时间格式
    @staticmethod
    def custom_time(model, num=0, time_delta=None) -> str:
        """
        自定义时间计算工具
        :param model: %Y-%m-%d %H:%M
        :param num: + - 天/小时/分钟
        :param time_delta: 默认 day 可选参数 hour min
        :return: 对应时间的字符串
        """
        if time_delta:
            time_delta = time_delta.lower()
        if time_delta is None:
            res = datetime.datetime.now() + timedelta(days=num)
            return res.strftime(model)
        elif time_delta == 'hour':
            res = datetime.datetime.now() + timedelta(hours=num)
            return res.strftime(model)
        elif time_delta == 'min':
            res = datetime.datetime.now() + timedelta(minutes=num)
            return res.strftime(model)
        else:
            raise "time_delta参数异常！"
