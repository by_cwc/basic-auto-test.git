# -*- coding: utf-8 -*-
# -------------------------------
# @文件：playwright_basic_page.py
# @时间：2025/2/26 16:52
# @作者：caiweichao
# @功能描述：
# -------------------------------
from playwright.sync_api import Page, expect, sync_playwright

from util.basic.analysis_yaml import AnalysisYaml
from util.basic.log import LogStoragerPocess, Log as log


class PlaywrightBasicPage:
    # 加载全局配置
    conf: dict = AnalysisYaml().get_date('PLAYWRIGHT_CONFIG')
    __img_dir = LogStoragerPocess().get_log_dir()


    def __init__(self, page: Page):
        self.page = page
        self.timeout = self.conf.get("TIMEOUT")
        self.poll_time = self.conf.get("POLL_TIME")

    def goto_url(self, url: str):
        """
        :param url: 跳转的指定url
        :return:
        """
        log.info(f"跳转url:{url}")
        self.page.goto(url)

