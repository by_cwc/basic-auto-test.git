# -*- coding: utf-8 -*-
# -------------------------------
# @文件：case_model.py
# @时间：2024/3/25 18:29
# @作者：caiweichao
# @功能描述：用例数据基类
# -------------------------------

class ApiTestCase:
    case_title: str = None
    url: str = None
    case_data: dict = None
    method: str = None
    check: list = None
    result: list = None
    env: str = None


class DubboTestCase:
    service: str = None
    service_name: str = None
    method_name: str = None
    case_title: str = None
    case_data = None
    jsonpath: list = None
    result: list = None
    account: dict = None
    more: bool = None
    load: str = None
