# -*- coding: utf-8 -*-
# -------------------------------
# @文件：custom_parameters.py
# @时间：2024/4/17 下午1:48
# @作者：caiweichao
# @功能描述：自定义参数配置，接口上下游，faker 数据
# -------------------------------
from util.basic.times import Times
from faker import Faker

faker = Faker(locale='zh_CN')
# 任何虚拟信息可以事先参考https://blog.csdn.net/weixin_43865008/article/details/115492280

custom_parameters = {
    'now': Times.custom_time(model=" %Y-%m-%d %H:%M:%S"),
    # 随机姓名
    'faker_name': faker.name,
    # 随机身份证号
    'faker_ssn': faker.ssn,
    # 随机职位
    'faker_job': faker.job,
}


def call_back(match):
    key = match.group(1)
    if key in custom_parameters:
        value =  custom_parameters[key]
        return value() if callable(value) else value
    else:
        return match.group(0)
