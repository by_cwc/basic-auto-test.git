# -*- coding: utf-8 -*-
# -------------------------------
# @文件：basic_request.py
# @时间：2024/4/22 下午2:58
# @作者：caiweichao
# @功能描述：接口测试调用的基类封装
# -------------------------------
from util.basic.http_request import HttpRequest
from util.basic.log import Log
from util.auto_test.api_test.case_model import ApiTestCase
from util.temporary_data import BasicData

import allure


class ApiTestRequest:
    def __init__(self, test_case: ApiTestCase, headers=None,cookies=None):
        Log.info(f"开始执行用例:{test_case.case_title}")
        # 如果需要登录，可以在这里优化下完成自己的登录逻辑
        if cookies:
            cookies = cookies
        else:
            cookies = getattr(BasicData, "cookies", None)

        Log.debug(f"从反射类中获取 cookies 获取结果为{cookies}")
        allure.dynamic.feature(f"RESTFUL接口测试")
        allure.dynamic.story(f"接口:{test_case.url}")
        allure.dynamic.title(test_title=test_case.case_title)
        if headers:
            self.response = HttpRequest(
                url=test_case.url,
                method=test_case.method,
                headers=headers,
                cookies=cookies,
                data=test_case.case_data)
        else:
            self.response = HttpRequest(
                url=test_case.url,
                method=test_case.method,
                cookies=cookies,
                data=test_case.case_data)

    def get_response(self):
        return dict(self.response.get_json())
