# -*- coding: utf-8 -*-
# -------------------------------
# @文件：customize_exception.py
# @时间：2024/4/16 下午4:58
# @作者：caiweichao
# @功能描述：自定义异常类
# -------------------------------
from util.basic.log import Log


class LenError(Exception):
    # 数据长度比较异常

    def __init__(self, message):
        self.message = message
        Log.error(message)
        super().__init__(self.message)


class AnalysisError(Exception):
    # 数据解析异常
    def __init__(self, message):
        self.message = message
        Log.error(message)
        super().__init__(self.message)
