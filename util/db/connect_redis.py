# -*- coding: utf-8 -*-
# -------------------------------
# @文件：connect_redis.py
# @时间：2024/4/9 下午2:37
# @作者：caiweichao
# @功能描述：链接 redis 数据库
# -------------------------------
import platform

import redis
from util.basic.analysis_yaml import AnalysisYaml


class ConnectRedis:
    __obj = None
    config = AnalysisYaml().get_date("REDIS_CONFIG")

    @staticmethod
    def __new__(cls, *args, **kwargs):
        if not cls.__obj:
            cls.__obj = super().__new__(cls)
        return cls.__obj

    def __init__(self):
        self.pool = redis.ConnectionPool(
            host=self.config["HOST"],
            port=self.config["PORT"],
            db=self.config["DB"],
            decode_responses=True,
        )
        self.redis = redis.Redis(connection_pool=self.pool)

    def set_value(self, name, key, value, time) -> redis:
        self.redis.hset(name, key, value)
        self.redis.expire(name=name, time=time)

    def get_value(self, key) -> dict:
        return self.redis.hgetall(key)
