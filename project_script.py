# -*- coding: utf-8 -*-
# -------------------------------
# @文件：project_script.py
# @时间：2024/3/22 11:43
# @作者：caiweichao
# @功能描述：项目中用到的一些小脚本和项目本身无关
# -------------------------------
import os

# 安装依赖
os.system('pip install -r requirements.txt')
# 更新 requeirments.txt
os.system('pip freeze > requirements.txt')

if __name__ == '__main__':
    pass
