# -*- coding: utf-8 -*-
# @Author  : caiweichao
# @explain : 测试夹具
import platform

import pytest
from selenium import webdriver

from util.basic.analysis_yaml import AnalysisYaml
from util.auto_test.ui_test.basic_page import BasicPage

config = AnalysisYaml().get_date('SELENIUM_CONFIG')


def pytest_collection_modifyitems(items):
    """
    测试用例收集完成时，将收集到的item的name和nodeid的中文显示在控制台上
    """
    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode_escape")
        item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")


def get_drvier(is_debug=False):
    global driver
    # 判断系统是否是linux如果是就返回true
    ishandless = True if platform.system() == 'Linux' else False
    option = webdriver.ChromeOptions()
    # 服务器使用或者远程调试 本地调试时is_debug改为 True
    if is_debug or ishandless:
        option.add_argument('--headless')
        option.add_argument('--no-sandbox')
        option.add_argument('--disable-gpu')
        option.add_argument('--disable-dev-shm-usage')
        option.add_argument('--hide-scrollbars')
        option.add_argument('--window-size=1920,1080')
        option.add_argument('--disable-blink-features=AutomationControlled')
        option.add_experimental_option('useAutomationExtension', False)
        option.add_experimental_option('excludeSwitches', ['enable-automation'])
        # command_executor 自己的 selenium grid 地址
        driver = webdriver.Remote(command_executor='http://127.0.0.1:5444/wd/hub', options=option)
        driver.implicitly_wait(config['ALL_TIMEOUT'])
    # 本地调试使用
    else:
        driver = webdriver.Chrome(service=webdriver.ChromeService(), options=option)
        driver.maximize_window()
    return driver


# 类级别登录测试夹具
@pytest.fixture(scope='class')
def login_class():
    _driver = get_drvier()
    # 封装自己的登录逻辑
    yield _driver
    driver.quit()


# 方法级别登录测试夹具
@pytest.fixture()
def login_fixture():
    _driver = get_drvier()
    # 封装自己的登录逻辑
    yield _driver
    BasicPage(driver).set_img_case()
    driver.quit()


@pytest.fixture()
def img():
    # 绑定在测试用例上会自动截图
    # 如果登录是fixture就不用使用这个装饰器，如果是 class 级别的登录装饰器需要在测试类上绑定次方法
    yield None
    BasicPage(driver).set_img_case()
