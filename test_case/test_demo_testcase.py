# -*- coding: utf-8 -*-
# -------------------------------
# @文件：test_demo_testcase.py
# @时间：2024/4/12 下午4:15
# @作者：caiweichao
# @功能描述：ui自动化 demo
# -------------------------------
import time
import pytest

from page_object.page_baidu_demo import PageBaidu
from util.auto_test.api_test.analysis_restful_testcase import AnalysisRestfulTestCase
from util.auto_test.api_test.basic_request import *
from util.auto_test.api_test.response_assert import ResponseAssert


# @pytest.mark.usefixtures('login_class')
@allure.feature('demo测试用例')
class TestDemoCase:
    def test_ui_baidu_demo1(self,login_fixture):
        PageBaidu(login_fixture).get_baidu()
        PageBaidu(login_fixture).input_keyword(keyword="gitee")
        PageBaidu(login_fixture).click_baidu_button()
        time.sleep(2)

    def test_ui_baidu_demo2(self, login_fixture):
        PageBaidu(login_fixture).get_baidu()
        PageBaidu(login_fixture).input_keyword(keyword="github")
        PageBaidu(login_fixture).click_baidu_button()
        time.sleep(2)

    @pytest.mark.parametrize("testcase", AnalysisRestfulTestCase("demo_service/demo_api.yaml").analysis_testcase())
    def test_demo_api(self, testcase: ApiTestCase):
        # 发起接口测试请求
        response = ApiTestRequest(testcase).get_response()
        # 断言校验
        assert ResponseAssert(testcase, response).assert_data() is True
