# -*- coding: utf-8 -*-
# @Author  : caiweichao
# @explain : page demo
from util.auto_test.ui_test.basic_page import BasicPage
from page_object.elements_demo import PageBaiduElements

import allure


class PageBaidu(BasicPage):
    # 实例化的百度定位元素
    element = PageBaiduElements()

    @allure.step("访问百度页面")
    def get_baidu(self):
        self.get_url(url=self.element.url)

    @allure.step("输入需要查询的关键字")
    def input_keyword(self, keyword):
        self.input_text(locator=self.element.input, content=keyword)

    @allure.step("点击百度一下")
    def click_baidu_button(self):
        self.click_element(locator=self.element.baiduButton)
