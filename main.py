# -*- coding: utf-8 -*-
# -------------------------------
# @文件：main.py
# @时间：2024/3/22 10:49
# @作者：caiweichao
# @功能描述：测试执行主入口
# -------------------------------
import os

from util.basic.log import Log


class Env:
    # 获取服务器当前环境
    env = os.environ.get('env')
    # 获取并发数
    thread = os.environ.get('thread')
    # 获取执行标记
    mark = os.environ.get('mark').split(',')


env = Env()


def mark():
    mark_len = len(env.mark)

    if mark_len > 1:
        mark = " or ".join(env.mark)
        return f'-m " {mark} "'
    if 'all' in env.mark or env.mark is None:
        return ''
    if mark_len == 1 and 'all' not in env.mark:
        mark = env.mark[0]
        return f" -m {mark} "


instruction = 'pytest -v --alluredir=${WORKSPACE}/Result/Report' + f' --clean-alluredir  {mark()} -n {env.thread} '
Log.info(f"当前环境:{Env.env}")
Log.info(f"执行命令：{instruction}")
os.system(instruction)
