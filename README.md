[![star](https://gitee.com/by_cwc/basic-auto-test/badge/star.svg?theme=dark)](https://gitee.com/by_cwc/basic-auto-test)
[![fork](https://gitee.com/by_cwc/basic-auto-test/badge/fork.svg?theme=dark)](https://gitee.com/by_cwc/basic-auto-test)

# 接口/UI通用框架持续集成版本

- 在原有的基础上大量的优化了代码，新增了持续集成方面的内容，美化了测试报告，
  将参数化执行从 excel 迁移到维护更加方便的 yaml 文件中，新增了多种断言方式在 yaml
  中可以直接配置，代码更加简洁清晰。大量的工具类重构更加方便的可以让你的项目从开源版本二开从而使用到公司项目中。
- 使用技术：Python+Selenium4+Requests+Allure2+Pytest+Jenkins+Pymysql+selenium4 Grid
- 我已经成功地在公司中落地了此框架，集成了大约 2w+的自动化 case，如果这个项目对你有帮助或启发，请多多点赞！
- 建议Python 版本 >= 3.9
- **希望各位大佬多多 star**
  - ![img.png](readme_img/111.png)

### 项目目录简介

1. [config](config) --> 存放配置文件
    1. [constants_path.py](config%2Fconstants_path.py) --> 项目对应文件夹路径
    2. [config.yaml](config%2Fconfig.yaml) --> 配置文件
2. [page_object](page_object) --> 页面对象管理
3. [test_case](test_case) --> 测试用例
   1. [conftest.py](test_case%2Fconftest.py) --> 测试用例公共夹具
   2. [test_demo_testcase.py](test_case/test_demo_testcase.py) --> demo用例
4. [test_data](test_data) --> 存放接口测试用例数据
5. [util](util) --> 工具类
   1. [auto_test](util%2Fauto_test) --> 自动化测试工具类
      1. [api_test](util%2Fauto_test%2Fapi_test) --> 接口自动化工具类
         1. [analysis_dubbo_testcase.py](util%2Fauto_test%2Fapi_test%2Fanalysis_dubbo_testcase.py) dubbo 用例解析工具类(开发中)
         2. [analysis_restful_testcase.py](util%2Fauto_test%2Fapi_test%2Fanalysis_restful_testcase.py) restful 用例解析工具类
         3. [basic_request.py](util%2Fauto_test%2Fapi_test%2Fbasic_request.py) 接口测试请求基础类
         4. [case_model.py](util%2Fauto_test%2Fapi_test%2Fcase_model.py) 用例模型
         5. [custom_parameters.py](util%2Fauto_test%2Fapi_test%2Fcustom_parameters.py) 自定义参数配置，接口上下游，faker 数据
         6. [response_assert.py](util%2Fauto_test%2Fapi_test%2Fresponse_assert.py) 接口响应断言
      2. [ui_test](util%2Fauto_test%2Fui_test) --> UI自动化工具类
         1. [basic_page.py](util%2Fauto_test%2Fui_test%2Fbasic_page.py) --> UI自动化基础类
      3. [get_allure_data.py](util%2Fauto_test%2Fget_allure_data.py) --> 获取 allure 报告数据
   2. [basic](util%2Fbasic) --> 基础工具类
      1. [analysis_json.py](util%2Fbasic%2Fanalysis_json.py) --> json解析
      2. [analysis_yaml.py](util%2Fbasic%2Fanalysis_yaml.py) --> yaml解析
      3. [dingding_request.py](util%2Fbasic%2Fdingding_request.py) --> 钉钉机器人
      4. [get_env.py](util%2Fbasic%2Fget_env.py) --> 获取环境变量
      5. [http_request.py](util%2Fbasic%2Fhttp_request.py) --> http请求基础类
      6. [log.py](util%2Fbasic%2Flog.py) --> 日志记录
      7. [times.py](util%2Fbasic%2Ftimes.py) --> 时间相关
   3. [db](util%2Fdb) --> 数据库操作
      1. [connect_mysql.py](util%2Fdb%2Fconnect_mysql.py) --> mysql数据库连接
      2. [connect_redis.py](util%2Fdb%2Fconnect_redis.py) --> redis数据库连接
   4. [customize_exception.py](util%2Fcustomize_exception.py) --> 自定义异常
   5. [temporary_data.py](util%2Ftemporary_data.py) --> 反射类临时数据
6. [jenkins_report.py](jenkins_report.py) --> jenkins 报告生成发送
7. [main.py](main.py) --> 项目入口
8. [project_script.py](project_script.py) --> 项目脚本
9. [requeirments.txt](requeirments.txt) --> 项目依赖包
10. [docker-compose.yml](docker-compose.yml) --> docker-compose

### 框架特色功能介绍

1. 支持 api用例 和 UI用例 的集成
2. ui 用例执行步骤染色功能,明确展示出来被操作元素
   1. ![img_4.png](readme_img/img_4.png)
3. 钉钉测试报告推送功能
   1. ![img_5.png](readme_img/img_5.png)
4. 接口自动化测试的allure报告动态写入不用自己定义 allure 的装饰器
5. 接口测试强化
   1. 支持多种断言模式，jsonpath，sql 断言，响应字段存在，响应字段不存在，接口参数替换，在扫描用例是将用例中的一些关键字替换为指定方法执行的结果。
      1. [demo_api.yaml](test_data%2Fdemo_service%2Fdemo_api.yaml)有详细的说明介绍
6. 可以支持 jenkins 持续集成，看后面的参考文档或者提issue
7. 持续集成主入口[main.py](main.py)，可以在 jenkins 上配置并发，环境，执行指定 mark，不用修改代码
8. [config.yaml](config%2Fconfig.yaml)通过 global 配置，在本地运行时切换环境

### 安装方式(本地运行)

1. 拉取项目安装requirements.txt中的包
2. 修改[config.yaml](config%2Fconfig.yaml)中的配置
3. 执行 demo 脚本保证正常运行,自此项目可以直接使用。

### jenkins集成与Selenium4 Grid集成

- 在本地运行成功之后，进一步的在公司内部集成，可以参考以下步骤：

#### jenkins 集成步骤

1. 在 jenkins 上创建一个自动化测试的 job，具体的步骤可以参考百度教程。
2. 新增文本参数 env (小写，可以设置默认值，pro/uat 来区分运行环境)
   ![img.png](readme_img/img.png)
3. 新增文本参数 thread 并发执行线程数初次配置建议为 1
   ![img.png](readme_img/img-1.png)
4. 新增执行标记参数 可以不配置为空，配置后将运行指定标记的用例
   ![img_1.png](readme_img/img_1.png)
5. 配置执行 shell (这段内容直接拷贝)
   ![img_2.png](readme_img/img_2.png)

```
#!/bin/bash
source /etc/profile
# 执行测试主要入口
python3 main.py  
# 初始化allure报告
allure generate ${WORKSPACE}/Result/Report -o ${WORKSPACE}/allure-report --clean || true
# 生成测试报告并且推送钉钉报告
python3 jenkins_report.py  || true
```

#### selenium grid 集成步骤

- 什么是selenium grid
- selenium grid 是一个分布式测试框架，可以运行在多台机器上，每个机器可以运行多个浏览器实例，
  并且可以动态的增加和减少机器。在 linux 上配置selenium grid，可以有效的隔离用例执行。

1. 在服务器上执行项目中的[docker-compose.yml](docker-compose.yml)
    2. 执行命令 docker-compose up -d
2. 出现这个图片代表安装成功，访问地址为 服务器ip:5444/ui/index.html#/
   ![img_3.png](readme_img/img_7.png)
3. 代码中配置selenium grid地址
4. 在 [conftest.py](test_case%2Fconftest.py) 修改配置

   ```
   #  将 ip 修改为你自己的服务器 ip 后面的端口号和地址不用改动 
   driver = webdriver.Remote(command_executor='http://127.0.0.1:5444/wd/hub', options=option)
   ```

3. 自此当你在 jenkins 上运行测试时，用例会自动分发到你创建的 selenium grid 上。

#### 备注

1. 不用自己安装chromedriver 会自动读取浏览器版本进行安装(会有点慢，如果不行可以写死 dirver 地址或者或尝试几次)

### 升级优化预告
新增 playwright 支持，playwright 部署集成测试方案